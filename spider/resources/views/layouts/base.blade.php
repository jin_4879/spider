<!DOCTYPE html>
<html lang=lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('layouts.partial.head')
    <body>
        <div id="app">
            <section>
                <nav>nav</nav>
                <main>@yield('content')</main>
                <footer>foot</footer>
            </section>
        </div>
        @include('layouts.partial.js')
    </body>
</html>