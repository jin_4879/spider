@extends('layouts.base')
@push('styles')
<style>
    form {
        margin: 10px 0;
    }

    .btn-search {
        background: #424242;
        border-radius: 0;
        color: #fff;
        border-width: 1px;
        border-style: solid;
        border-color: #1c1c1c;
    }

    .btn-search:link,
    .btn-search:visited {
        color: #fff;
    }

    .btn-search:active,
    .btn-search:hover {
        background: #1c1c1c;
        color: #fff;
    }

    img {
        width: 150px;
        height: 150px;
    }

    .table thead th {
        white-space: nowrap;
        text-align: center;
    }

    .highlight {
        background-color: yellow
    }

    .fixed-td {
        word-break: break-word;
        min-width: 150px;
    }

    .media-container {
        display: flex;
        flex-flow: nowrap;
        overflow: scroll;
        width: 190px;
        height: 200px;
        padding: 25px 0px;
    }

    .media-cover {
        width: 150px;
        position: relative;
        word-break: break-word;
    }

    .thumbnail-img {
        opacity: 1;
        display: block;
        transition: .5s ease;
        backface-visibility: hidden;
    }

    .top {
        opacity: 1;
        position: absolute;
        top: 10px;
        right: 10px;
        color: white;
    }

    .middle {
        position: absolute;
        width: 150px;
        height: 150px;
        top: 0;
        background-color: rgba(0, 0, 0, 0.4);
        transition: .5s ease;
        opacity: 0;
        text-align: left;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .media-cover:hover .middle {
        opacity: 1;
    }

    .text {
        color: white;
        font-size: 16px;
    }
</style>
@endpush
@section('content')
<div class="container">
    <h2>인스타그램 계정수 : @comma($InstagramInfo->total()) / @comma($instagram_count)</h2>
    <form action="/" method="get" accept-charset="UTF-8">
        <div class="input-group">
            <input type="text" class="form-control" name="search_text" placeholder="Search for..." value="{{$query ?? ''}}">
            <span class="input-group-btn">
                <button class="btn btn-search" type="submit"><i class="fa fa-search fa-fw"></i> Search</button>
            </span>
        </div>
    </form>
    <div class="table-responsive">
        <table class="table table-bordered highlight-area">
            <thead>
                <tr>
                    <th scope="col">아이디</th>
                    <th scope="col">이름</th>
                    <th scope="col">프로필 이미지</th>
                    <th scope="col" style="min-width: 100px;">최근 미디어</th>
                    <th scope="col">팔로워</th>
                    <th scope="col">팔로잉</th>
                    <th scope="col">게시물</th>
                    <th scope="col">소개글</th>
                    <th scope="col">언어</th>
                    <th scope="col">외부링크</th>
                    <th scope="col">인증</th>
                    <th scope="col">공개</th>
                    <th scope="col" style="min-width: 100px;">업데이트</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($InstagramInfo as $item)
                <tr>
                    <td><a href="https://www.instagram.com/{{$item->id}}" target="_blank">{{$item->id}}</a></td>
                    <td class="fixed-td">{{$item->full_name}}</td>
                    <td><img src="{{$item->profile_pic_url}}" alt="프로필 이미지" /></td>
                    <td>
                        <div class="media-container">
                            @foreach ($item->media as $medium)
                            @php ($medium_info = $medium['node'])
                            @php ($is_video = $medium_info['is_video'])
                            @php ($typename = $medium_info['__typename'])
                            <a class="media-cover" href="https://www.instagram.com/p/{{$medium_info['shortcode']}}/" target="_blank">
                                <img class="thumbnail-img" src="{{$medium_info['thumbnail_resources'][0]['src']}}" alt="미디어 썸네일">
                                <div class="top">
                                    @if ($is_video === true)
                                    <i class="fas fa-video"></i>
                                    @elseif ($typename === 'GraphSidecar')
                                    <i class="fas fa-clone"></i>
                                    @endif
                                </div>
                                <div class="middle">
                                    <div class="text">
                                        @if ($is_video)
                                        <p class="m-0"><i class="fas fa-play"></i><span class="ml-1">@comma($medium_info['video_view_count'])</span></p>
                                        @endif
                                        <p class="m-0"><i class="fas fa-heart"></i><span class="ml-1">@comma($medium_info['edge_media_preview_like']['count'])</span></p>
                                        <p class="m-0"><i class="fas fa-comment"></i><span class="ml-1">@comma($medium_info['edge_media_to_comment']['count'])</span></p>
                                        <p class="m-0"><i class="fas fa-clock"></i><span class="ml-1">{{Carbon::createFromTimestamp($medium_info['taken_at_timestamp'])->format('y-m-d')}}</span></p>
                                    </div>
                                </div>
                                <p class="p-2">{{$medium_info['edge_media_to_caption']['edges'][0]['node']['text'] ?? ''}}</p>
                            </a>
                            @endforeach
                        </div>
                    </td>
                    <td class="text-right">@comma($item->followed_count)</td>
                    <td class="text-right">@comma($item->follow_count)</td>
                    <td class="text-right">@comma($item->media_count)</td>
                    <td class="fixed-td">{!!$item->biography_with_href!!}</td>
                    <td class="text-nowrap">{{$item->extra->lang ?? ''}}</td>
                    <td class="text-center">
                        @if ($item->external_url)
                        <a href="{{$item->external_url}}" target="_blank">url</a>
                        @endif
                    </td>
                    <td class="text-center">
                        @if ($item->is_verified)
                        <i class="fas fa-check-circle"></i>
                        @else
                        <i class="far fa-times-circle"></i>
                        @endif
                    </td>
                    <td class="text-center">
                        @if ($item->is_private)
                        <i class="far fa-times-circle"></i>
                        @else
                        <i class="fas fa-check-circle"></i>
                        @endif
                    </td>
                    <td>{{$item->updated_at ?? $item->created_at}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $InstagramInfo->appends(Request::all())->links() }}
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(function(){
        let q = @json($query ?? '');
        console.log(q)
        if(q){
            $('.highlight-area').highlight(q);
        }
    })
</script>
@endpush
