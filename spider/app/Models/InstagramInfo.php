<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstagramInfo extends Model
{
    protected $table = 'instagram_info';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    protected $appends = [
        'biography_with_href',
        'media',
    ];

    public $incrementing = false;

    public function getBiographyWithHrefAttribute()
    {
        return preg_replace('/#(.*) /U', '<a href="https://www.instagram.com/explore/tags/$1" target="_blank">#$1</a> ', $this->biography);
    }

    public function getMediaAttribute()
    {
        return json_decode($this->media_edges, true);
    }

    public function extra()
    {
        return $this->hasOne(InstagramExtraInfo::class, 'id', 'id');
    }
}
