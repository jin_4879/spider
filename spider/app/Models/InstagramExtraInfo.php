<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InstagramExtraInfo extends Model
{
    protected $table = 'instagram_extra_info';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    protected $appends = [
        //
    ];

    public $incrementing = false;
}
