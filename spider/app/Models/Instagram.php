<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Instagram extends Model
{
    protected $table = 'instagram';
    protected $primaryKey = 'id';
    protected $keyType = 'string';

    public $incrementing = false;

    public function info()
    {
        return $this->hasOne(InstagramInfo::class, 'id', 'id');
    }
}
