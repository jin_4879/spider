<?php

namespace App\Http\Controllers;

use DateTime;
use App\Models\Instagram;
use App\Models\InstagramInfo;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    public function index()
    {
        $search_text = request('search_text');
        $InstagramInfo = InstagramInfo::query();
        if ($search_text !== null) {
            $columns = ['id', 'full_name', 'biography', 'extra.lang'];
            if ($this->validateDate($search_text, 'Y-m-d')) {
                array_push($columns, 'created_at');
            }
            $InstagramInfo->whereLike($columns, $search_text);
        }
        $InstagramInfo = $InstagramInfo->orderBy('followed_count', 'DESC')->paginate(5);

        return view('home', [
            'instagram_count' => Instagram::count(),
            'InstagramInfo' => $InstagramInfo,
            'query' => $search_text,
        ]);
    }
}
