# Instagram Crawling

#### Structure
* folder
  * spider : laravel web service
  * spider_py : python instagram crawling
* services
  * Nginx
  * Mysql
  * Python
  * php

#### Install
1. Download Docker
1. Source Clone Or Download
1. set .env instagram id and password
1. `docker-compose up -d`
1. create tables
1. `docker exec -it -u root spider-python bash`
  1. get instagram id `python get_id.py`
  1. get instagram info `python get_info.py`
  1. add instagram extra info `python save_extra_info.py`

#### Precautions
* 크롤링에 사용되는 인스타 아이디는 새로 만드세요.
* 아이디당 하루 최대 약 480개 정보를 가져옵니다.
* 여러 아이디로 크롤링 할경우 `app.py` 를 사용하세요.


```
CREATE TABLE `instagram` (
	`id` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)
COLLATE='utf8mb4_unicode_ci'
ENGINE=InnoDB;
```

```
CREATE TABLE `instagram_extra_info` (
	`id` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`lang` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)
COLLATE='utf8mb4_unicode_ci'
ENGINE=InnoDB;
```

```
CREATE TABLE `instagram_info` (
	`id` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`full_name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`user_name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`profile_pic_url` LONGTEXT NULL COLLATE 'utf8mb4_unicode_ci',
	`followed_count` BIGINT(20) UNSIGNED NULL DEFAULT NULL,
	`follow_count` BIGINT(20) UNSIGNED NULL DEFAULT NULL,
	`media_count` BIGINT(20) UNSIGNED NULL DEFAULT NULL,
	`external_url` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`biography` LONGTEXT NULL COLLATE 'utf8mb4_unicode_ci',
	`media_edges` JSON NULL DEFAULT NULL,
	`is_verified` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`is_private` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci',
	`created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` DATETIME NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)
COLLATE='utf8mb4_unicode_ci'
ENGINE=InnoDB;
```

```
CREATE TABLE `instagram_no_data` (
	`id` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
)
COLLATE='utf8mb4_unicode_ci'
ENGINE=InnoDB;
```