import sys, requests, json, pymysql, random, string, os, re, traceback

from bs4 import BeautifulSoup
from selenium import webdriver
from random import randint
from time import sleep
from Log import Log

logger = Log.getLogger()

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--window-size=1420,1080')
chrome_options.add_argument('--headless')
chrome_options.add_argument('--disable-gpu')
chrome_options.add_argument('--disable-dev-shm-usage')
chrome_options.add_argument('lang=ko_KR')
chrome_options.add_argument('user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36')
driver = webdriver.Chrome(options=chrome_options)

driver.implicitly_wait(3)
driver.delete_all_cookies()

db = pymysql.connect(host='db', port=3306, user='root', passwd=os.getenv('MYSQL_ROOT_PASSWORD'), db='spider')
cursor = db.cursor()

try:
    for i in range(100):
        sleep(randint(10, 100))
        character = ''.join(random.choice(string.ascii_lowercase + string.digits) for i in range(randint(2,5)))
        driver.get('https://www.instagram.com/web/search/topsearch/?query=' + character)
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        dict_from_json = json.loads(soup.find("body").text)
        for user in dict_from_json['users']:
            sql = '''INSERT IGNORE INTO `instagram` (id) VALUES (%s);'''
            cursor.execute(sql, (user['user']['username']))
            print(user['user']['username'])
        db.commit()

except Exception as e:
    logger.error(traceback.format_exc())
    if 'user' in locals():
        logger.info(user)

finally:
    cursor.close()
    db.close()