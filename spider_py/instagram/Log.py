import logging
import logging.handlers

class Log:
    logger = logging.getLogger(__name__)
    formatter = logging.Formatter('[%(asctime)s][%(levelname)s|%(filename)s:%(lineno)s] %(message)s')

    streamHandler = logging.StreamHandler()
    fileHandler = logging.FileHandler('./pylog.log')

    streamHandler.setFormatter(formatter)
    fileHandler.setFormatter(formatter)

    logger.addHandler(streamHandler)
    logger.addHandler(fileHandler)

    logger.setLevel(level=logging.DEBUG)

    def getLogger():
        return Log.logger

    # logger.debug('my DEBUG log')
    # logger.info('my INFO log')
    # logger.warning('my WARNING log')
    # logger.error('my ERROR log')
    # logger.critical('my CRITICAL log')