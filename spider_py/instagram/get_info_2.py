import sys, requests, json, pymysql, random, string, os, re, traceback, datetime

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from random import randint
from time import sleep
from Log import Log

logger = Log.getLogger()
logger.info('start')

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--window-size=1420,1080')
chrome_options.add_argument('--headless')
chrome_options.add_argument('--disable-gpu')
chrome_options.add_argument('--disable-dev-shm-usage')
chrome_options.add_argument('lang=ko_KR')
chrome_options.add_argument('user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36')
chrome_options.add_argument("proxy-server=socks5://localhost:9050")
driver = webdriver.Chrome(options=chrome_options)

driver.implicitly_wait(3)
driver.delete_all_cookies()

driver.get('http://icanhazip.com/')
logger.info("using ip : " + driver.find_element_by_tag_name("pre").text)

db = pymysql.connect(host='db', port=3306, user='root', passwd=os.getenv('MYSQL_ROOT_PASSWORD'), db='spider')
cursor = db.cursor()

try:
    def checkBan(chrome_driver):
        is_error_text = chrome_driver.find_elements_by_tag_name('h2')[0].text == 'Error' if chrome_driver.find_elements_by_tag_name('h2') else False
        is_error_test_p = chrome_driver.find_elements_by_tag_name('p')[0].text == 'Please wait a few minutes before you try again.' if chrome_driver.find_elements_by_tag_name('p') else False
        return is_error_text and is_error_test_p

    driver.get('https://www.instagram.com')
    i = 10
    while checkBan(driver) and i > 1:
        logger.info('login ban.. cnt : ' + str(i))
        driver.implicitly_wait(10)
        driver.delete_all_cookies()
        driver.get('https://www.instagram.com')
        i = i - 1
    driver.implicitly_wait(30)
    driver.find_element(By.NAME, 'username').send_keys(os.getenv('INSTAGRAM_ID').split(',')[1])
    driver.find_element(By.NAME, 'password').send_keys(os.getenv('INSTAGRAM_PASSWORD').split(',')[1])
    driver.find_element(By.XPATH, '//button[@type="submit"]').click()
    cursor.execute('''SELECT a.id AS id FROM instagram a
		                LEFT JOIN instagram_info b ON a.id = b.id
		                LEFT JOIN instagram_no_data c ON a.id = c.id
		                WHERE b.id IS NULL AND c.id IS NULL ORDER BY a.id desc;''')
    result = cursor.fetchall()
    insert_count = 0
    continue_ids = []
    for i, [ins_id] in enumerate(result):
        sleep(randint(5, 10))
        driver.get('https://www.instagram.com/' + ins_id)
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        script = soup.find("script",text=re.compile(r"\"graphql\""))
        if script == None:
            if(checkBan(driver)):
                logger.info('ban.. : ' + ins_id)
                break
            if len(continue_ids) >= 5:
                sql = '''DELETE FROM `instagram_no_data` WHERE id =%s;'''
                cursor.executemany(sql,tuple(continue_ids))
                db.commit()
                logger.info('continue Rows Deleted : ' + str(continue_ids))
                logger.error(driver.page_source)
                break
            print('continue')
            sql = '''INSERT IGNORE INTO `instagram_no_data` (id) VALUES (%s);'''
            cursor.execute(sql, ins_id)
            db.commit()
            continue_ids.append(ins_id)
            continue
        json_data = json.loads(re.search(r"window._sharedData = (.*);",script.string).group(1))
        user = json_data['entry_data']['ProfilePage'][0]['graphql']['user']

        insert_data = {
            'id' : ins_id,
            'biography': user['biography'],
            'external_url': user['external_url'],
            'followed_count': user['edge_followed_by']['count'],
            'follow_count': user['edge_follow']['count'],
            'full_name': user['full_name'],
            'is_verified': user['is_verified'],
            'is_private': user['is_private'],
            'profile_pic_url': user['profile_pic_url'],
            'user_name': user['username'],
            'media_count': user['edge_owner_to_timeline_media']['count'],
            'media_edges': json.dumps(user['edge_owner_to_timeline_media']['edges']),
        }

        sql = '''INSERT INTO `instagram_info`
            (id, full_name, user_name, profile_pic_url, followed_count, follow_count, media_count, external_url, biography, media_edges, is_verified, is_private)
            VALUES (%(id)s, %(full_name)s, %(user_name)s, %(profile_pic_url)s, %(followed_count)s, %(follow_count)s, %(media_count)s,
                %(external_url)s, %(biography)s, %(media_edges)s, %(is_verified)s, %(is_private)s)
            ON DUPLICATE KEY UPDATE
                full_name=%(full_name)s, user_name=%(user_name)s, profile_pic_url=%(profile_pic_url)s, followed_count=%(followed_count)s,
                follow_count=%(follow_count)s, media_count=%(media_count)s, external_url=%(external_url)s, biography=%(biography)s,
                media_edges=%(media_edges)s, is_verified=%(is_verified)s, is_private=%(is_private)s;'''
        cursor.execute(sql, insert_data)
        insert_count = insert_count + 1
        continue_ids = []
        db.commit()
        print('inserted id : ' + ins_id + ' count :' + str(insert_count))
        if i > 500:
            break

except Exception as e:
    logger.error(traceback.format_exc())
    if 'ins_id' in locals():
        logger.info(ins_id)
    logger.error(driver.page_source)

finally:
    logger.info('end')
    if 'insert_count' in locals():
        logger.info('inserted_count :' + str(insert_count))
    driver.delete_all_cookies()
    driver.close()
    driver.quit()
    cursor.close()
    db.close()