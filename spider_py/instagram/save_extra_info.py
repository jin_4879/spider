import sys, requests, json, pymysql, random, string, os, re, traceback

from random import randint
from time import sleep
from Log import Log
from googletrans import Translator, LANGUAGES

translator = Translator()
logger = Log.getLogger()
logger.info('start')

db = pymysql.connect(host='db', port=3306, user='root', passwd=os.getenv('MYSQL_ROOT_PASSWORD'), db='spider')
cursor = db.cursor()

try:
    cursor.execute('''SELECT a.id, a.media_edges FROM instagram_info a LEFT JOIN instagram_extra_info b ON a.id = b.id WHERE b.id IS NULL AND JSON_LENGTH(a.media_edges) > 0;''')
    result = cursor.fetchall()
    result_len = len(result)
    insert_count = 0
    for i, [ins_id,media_edges] in enumerate(result):
        sleep(3)
        media_edges = json.loads(media_edges)
        if not media_edges:
            continue
        list_lan = []
        for medium in media_edges:
            if not medium['node']['edge_media_to_caption']['edges']:
                continue
            text = medium['node']['edge_media_to_caption']['edges'][0]['node']['text']
            if len(text) < 3:
                continue
            lang = LANGUAGES.get(translator.detect(text).lang.lower())
            if lang == None:
                continue
            list_lan.append(lang)
        insert_lan = ','.join(list(set(list_lan)))
        sql = '''INSERT INTO `instagram_extra_info` (id,lang) VALUES (%s,%s) ON DUPLICATE KEY UPDATE lang=%s;'''
        cursor.execute(sql, (ins_id, insert_lan, insert_lan))
        insert_count = insert_count + 1
        print('remain cnt: ' + str(result_len - insert_count))
        db.commit()

except Exception as e:
    logger.error(traceback.format_exc())
    if 'ins_id' in locals():
        logger.info(ins_id)

finally:
    logger.info('end inserted count :' + str(insert_count))
    cursor.close()
    db.close()
