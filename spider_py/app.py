from subprocess import call
import multiprocessing

def worker(file):
    call(["python", "/usr/src/app/" + file])

files = ["instagram/get_info.py","instagram/get_info_2.py","instagram/save_extra_info.py"]
for i in files:
    p = multiprocessing.Process(target=worker, args=(i,))
    p.start()